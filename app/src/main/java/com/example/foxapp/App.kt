package com.example.foxapp

import android.app.Application
import com.example.foxapp.data.FoxRepository
import com.example.foxapp.network.FoxDataSource
import com.example.foxapp.network.SharedPreferencesDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class App : Application() {

    val repository by lazy { FoxRepository(FoxDataSource(), SharedPreferencesDataSource()) }

    override fun onCreate() {
        super.onCreate()
        appContext = this
        appRepository = repository

    }

    override fun onTerminate() {
        super.onTerminate()
    }

    companion object {
        lateinit var appContext: App
            private set
        lateinit var appRepository: FoxRepository
            private set
    }
}