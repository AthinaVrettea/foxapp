package com.example.foxapp

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.foxapp.databinding.ActivityMainBinding
import com.example.foxapp.ui.FoxViewModel
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private val foxViewModel: FoxViewModel by viewModels { FoxViewModel.Factory }

    private var actionFavorites: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                foxViewModel.foxUiState.collect {
                    if(it.isFavorite){
                        actionFavorites?.icon = resources.getDrawable(R.drawable.ic_action_favorites)
                    } else {
                        actionFavorites?.icon = resources.getDrawable(R.drawable.ic_heart)
                    }
                }
            }
        }
        binding.fab.setOnClickListener { view ->
            foxViewModel.fetchFox()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        actionFavorites = menu.findItem(R.id.action_favorites)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_show_favorite -> {
                //foxViewModel.filterFoxes(true)
                val favorite = foxViewModel.getFavorite()
                Toast.makeText(App.appContext,"Favorite Fox so far is $favorite",
                    Toast.LENGTH_LONG).show()
                true
            }
            R.id.action_favorites -> {
                foxViewModel.toggleFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }
}