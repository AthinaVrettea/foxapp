package com.example.foxapp.domain.model

import android.media.Image

data class Fox(
    var image: String,
)
