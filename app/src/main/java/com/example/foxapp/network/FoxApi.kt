package com.example.foxapp.network

import com.example.foxapp.domain.model.Fox
import retrofit2.http.GET

interface FoxApi {

    @GET("floof/?ref=apilist.fun")
    suspend fun getFox(): Fox

}