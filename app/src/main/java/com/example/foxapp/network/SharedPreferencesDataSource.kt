package com.example.foxapp.network

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import com.example.foxapp.App

class SharedPreferencesDataSource(private val context: Context = App.appContext) {

    private lateinit var preferences: SharedPreferences
    private val sharedPrefFile: String = "com.example.foxapp"
    private val FOX_KEY = "fox"

    fun storeFavorite(foxLink: String) {
        preferences = context.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.putString(FOX_KEY, foxLink)
        editor.apply()

    }

    fun removeFavorite(foxLink: String) {
        preferences = context.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.remove(foxLink)
        editor.apply()
    }

    fun getFavorite(): String? {
        preferences = context.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        preferences.getString(FOX_KEY, "fox")?.let {
            Log.d("fav", it) }
        val favoriteFox = preferences.getString(FOX_KEY, "fox")
        return favoriteFox
    }

}