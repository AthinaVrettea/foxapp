package com.example.foxapp.network

import com.example.foxapp.domain.model.Fox

class FoxDataSource {

    suspend fun getFox(): Fox {
       return RetrofitService.getFoxApi().getFox()
    }
}