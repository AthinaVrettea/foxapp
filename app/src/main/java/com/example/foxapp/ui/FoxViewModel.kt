package com.example.foxapp.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.foxapp.data.FoxRepository
import com.example.foxapp.domain.model.Fox
import com.example.foxapp.network.FoxDataSource
import com.example.foxapp.network.SharedPreferencesDataSource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class FoxViewModel(private val foxRepository: FoxRepository) : ViewModel() {

    private val _foxUiState = MutableStateFlow<FoxUiState>(FoxUiState(Fox("")))
    val foxUiState: StateFlow<FoxUiState> = _foxUiState

    fun fetchFox() {
        _foxUiState.update { foxUiState -> foxUiState.copy(isLoading = true) }
        viewModelScope.launch {
            foxRepository.getFox().collect {
                _foxUiState.update { foxUiState ->
                    foxUiState.copy(
                        fox = it,
                        isLoading = false, isFavorite = false
                    )
                }
            }
        }
    }

    fun filterFoxes(filter: Boolean) {
        _foxUiState.update { foxUiState -> foxUiState.copy(isChecked = filter) }
    }

    fun toggleFavorite() {
        val favorite = !_foxUiState.value.isFavorite
        if (favorite) {
            foxRepository.storeFavorite(_foxUiState.value.fox.image)
        } else {
            foxRepository.removeFavorite(_foxUiState.value.fox.image)
        }
        _foxUiState.update { foxUiState ->
            foxUiState.copy(isFavorite = !foxUiState.isFavorite)
        }
    }

    fun getFavorite(): String? {
        val favorite = foxRepository.getFavorite()
        return favorite
    }
    
    companion object {
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                FoxViewModel(
                    FoxRepository(FoxDataSource(), SharedPreferencesDataSource())
                )
            }
        }
    }
}

data class FoxUiState(
    val fox: Fox,
    val isChecked: Boolean = false,
    val isLoading: Boolean = true,
    var isFavorite: Boolean = false
)