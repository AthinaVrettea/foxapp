package com.example.foxapp

import android.annotation.SuppressLint
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.foxapp.databinding.FragmentFirstBinding
import com.example.foxapp.ui.FoxViewModel
import kotlinx.coroutines.launch

class FirstFragment : Fragment() {

    private val foxViewModel: FoxViewModel by activityViewModels { FoxViewModel.Factory }
    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED){
                foxViewModel.foxUiState.collect {
                    if(it.isLoading) {
                        binding.foxLink.setText("Press the button to see a fox")
                        binding.foxImage.setImageResource(R.drawable.starter_fox)
                    } else {
                        Glide.with(requireContext()).load(it.fox.image).into(binding.foxImage)
                        binding.foxLink.setText(it.fox.image.toString())
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}