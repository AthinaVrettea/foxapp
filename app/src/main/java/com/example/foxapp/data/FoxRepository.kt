package com.example.foxapp.data

import com.example.foxapp.domain.model.Fox
import com.example.foxapp.network.FoxDataSource
import com.example.foxapp.network.SharedPreferencesDataSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FoxRepository(private val foxDataSource: FoxDataSource,
                    private val sharedPreferencesDataSource: SharedPreferencesDataSource) {

    suspend fun getFox(): Flow<Fox> = flow {
        emit(foxDataSource.getFox())
    }

    fun storeFavorite(foxLink: String) {
        sharedPreferencesDataSource.storeFavorite(foxLink)
    }

    fun removeFavorite(foxLink: String) {
        sharedPreferencesDataSource.removeFavorite(foxLink)
    }

    fun getFavorite(): String? {
        val favorite = sharedPreferencesDataSource.getFavorite()
        return favorite
    }
}